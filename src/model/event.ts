import Player from './player';

export default class Event {
	public static LOGINTYPE: string = "LOGIN";
	public static LOGOUTTYPE: string = "LOGOUT";
	public static UNKNOWTYPE: string = "UNKNOW";

	private date: Date;
	private player: Player;

	private type: string;
	private trueLog: string;


	constructor(dateParam:Date, playerParam: Player, typeParam: string, trueLogParam: string) {
		this.date = dateParam;
		this.player = playerParam;
		this.type = typeParam;
		this.trueLog = trueLogParam;
	}

	private setDate(dateParam: Date): void {
		this.date = dateParam;
	}

	private setPlayer(playerParam: Player): void {
		this.player = playerParam;
	}

	private setType(typeParam: string): void {
		this.type = typeParam;
	}

	public getEventDate(): Date {
		return this.date;
	}

	public getPlayer(): Player {
		return this.player;
	}

	public getType(): string {
		return this.type;
	}

	public getTrueLog(): string {
		return this.trueLog;
	}
}
