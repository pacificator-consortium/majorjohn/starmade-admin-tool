import Event from './event';

export default class Player {	
	private accountName: string;
	private pseudos: string[];
	private events: Event[];

	constructor(accountNameParam: string) {
		this.accountName= accountNameParam;
		this.events = []
		this.pseudos = [];
	}

	public getEvents(): Event[] {
		return this.events;
	}

	public getPseudos(): string[] {
		return this.pseudos;
	}

	public getAccountName(): string {
		return this.accountName;
	}
}
