export default class Session {
	private dateLogIn: Date;
	private dateLogOut: Date;

	//Session time
	private days: number;
	private hours: number;
	private minutes: number;
	private seconds: number;


	constructor(dateLogInParam: Date, dateLogOutParam: Date) {
		this.dateLogIn = dateLogInParam;
		this.dateLogOut = dateLogOutParam;
		this.days = 0;
		this.hours = 0;
		this.minutes = 0;
		this.seconds = 0;


		// Set the unit values in milliseconds.
		var msecPerMinute = 1000 * 60;
		var msecPerHour = msecPerMinute * 60;
		var msecPerDay = msecPerHour * 24;

		// Get the difference in milliseconds.
		var interval = this.dateLogOut.getTime() - this.dateLogIn.getTime();

		this.days = Math.floor(interval / msecPerDay );
		interval = interval - (this.days * msecPerDay );

		this.hours = Math.floor(interval / msecPerHour );
		interval = interval - (this.hours * msecPerHour );

		this.minutes = Math.floor(interval / msecPerMinute );
		interval = interval - (this.minutes * msecPerMinute );

		this.seconds = Math.floor(interval / 1000 );
	}

	public toString(): string {
		return this.dateLogIn.getDate() + "/" + (this.dateLogIn.getMonth()+1) + "/" + this.dateLogIn.getFullYear() + " : " + this.days + " jours, " + this.hours + " heures, " + this.minutes + " minutes, " + this.seconds + " secondes"
	}
}
