import Main from './main';
import {ipcMain} from 'electron';
import FtpControlServices from './../internal/ftpControlServices';
import StatControlServices from './../internal/statControlServices';
import BackupViewControl from './backupViewControl';

export default class SomeViewControl {

	private ftpControlServices: FtpControlServices;
    private statControlServices: StatControlServices;
    private backupViewControl: BackupViewControl;

    constructor(ftpControlServicesParam: FtpControlServices, statControlServicesParam: StatControlServices,
        backupViewControlParam: BackupViewControl){
    	this.ftpControlServices = ftpControlServicesParam;
        this.statControlServices = statControlServicesParam;
        this.backupViewControl = backupViewControlParam;
    }

    public loadHtml(){
    	Main.mainWindow.loadURL('file://' + __dirname + '/../../view/index.html');
        ipcMain.on("downloadLogs", (event) => { this.onClickGetLogs(); });
        ipcMain.on("loadPlayer", (event) => { this.onProcessStatLogs(); });
        ipcMain.on("backupTool", (event) => { this.onClickBackupTool(); });
    }

    public async onClickGetLogs(): Promise<void> {
    	await this.ftpControlServices.getAllFiles([FtpControlServices.LOG_NAME_FILTER],
            [],
            FtpControlServices.FTP_LOGS_FOLDER, 
            FtpControlServices.LOCAL_LOGS_FOLDER, 
            this.updatePercent);
        //Clean Progress Bar
    }

    public onClickBackupTool(): void {
        this.backupViewControl.loadHtml();
    }

    public onProcessStatLogs() {
        this.statControlServices.writeFilteredLogs("logs/");
    }

    public updatePercent(percent: number) {
        Main.mainWindow.webContents.send('percentUpdate', percent + "%");  
    }
}
