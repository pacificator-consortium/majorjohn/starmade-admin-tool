import {ipcMain} from 'electron';
import { getLogger, Logger } from 'log4js';

import Main from './main';
import FtpControlServices from './../internal/ftpControlServices';
import CommonControlServices from './../internal/commonControlServices';
import ConfigDataServices from './../internal/configDataServices';

export default class BackupViewControl {

	private static LOCAL_BACKUP_DATA_FOLDER: string = "/data";
	private static LOCAL_BACKUP_INDEX_FOLDER: string = "/index";

	/** map folder filter */
	private static PLAYER_NAME_FILTER: RegExp = /ENTITY_PLAYERCHARACTER|ENTITY_PLAYERSTATE.*[.]ent/;
	private static FILTER_ASTEROID: RegExp = /ENTITY_FLOATINGROCKMANAGED|ENTITY_FLOATINGROCK.*[.]ent/;
	private static FILTER_PLANET: RegExp = /ENTITY_PLANETCORE|ENTITY_PLANET.*[.]ent/;
	private static FILTER_SHIP_NPC: RegExp = /ENTITY_SHIP_FLTSHP|ENTITY_SHIP_GFLTSHP.*[.]ent/;
	private static FILTER_STATION_NEUTRE: RegExp = /ENTITY_SHIP_Station_Derelict|ENTITY_SPACESTATION_Station_Derelict.*[.]ent/;
	private static FILTER_STATION_PIRATE: RegExp = /ENTITY_SHIP_Station_Piratestation|ENTITY_SPACESTATION_Station_Piratestation.*[.]ent/;
	private static FILTER_STATION_TRADE: RegExp = /ENTITY_SHIP_Station_Tradestation|ENTITY_SPACESTATION_Station_Tradestation.*[.]ent/;
	private static FILTER_SHOP: RegExp = /ENTITY_SHIP_Trade.*[.]ent/;
	private static FILTER_FLEET: RegExp = /ENTITY_SHIP_\[System Fleet.*[.]ent/;

	/** data folder filter */
	private static FILTER_DATA_ASTEROID: RegExp = /ENTITY_FLOATINGROCKMANAGED|ENTITY_FLOATINGROCK.*[.]smd3/;
	private static FILTER_DATA_PLANET: RegExp = /ENTITY_PLANET.*[.]smd3/;
	private static FILTER_DATA_SHIP_NPC: RegExp = /ENTITY_SHIP_FLTSHP|ENTITY_SHIP_GFLTSHP.*[.]smd3/;
	private static FILTER_DATA_STATION_NEUTRE: RegExp = /ENTITY_SHIP_Station_Derelict|ENTITY_SPACESTATION_Station_Derelict.*[.]smd3/;
	private static FILTER_DATA_STATION_PIRATE: RegExp = /ENTITY_SHIP_Station_Piratestation|ENTITY_SPACESTATION_Station_Piratestation.*[.]smd3/;
	private static FILTER_DATA_STATION_TRADE: RegExp = /ENTITY_SHIP_Station_Tradestation|ENTITY_SPACESTATION_Station_Tradestation.*[.]smd3/;
	private static FILTER_DATA_FLEET: RegExp = /ENTITY_SHIP_\[System Fleet.*[.]ent/;

	private ftpControlServices: FtpControlServices;
	private commonControlServices: CommonControlServices;

	private logger: Logger;


	constructor(ftpControlServicesParam: FtpControlServices, commonControlServicesParam: CommonControlServices){
		this.logger = getLogger("global");
		this.ftpControlServices = ftpControlServicesParam;
		this.commonControlServices = commonControlServicesParam;
	}

	public loadHtml(){
		Main.mainWindow.loadURL('file://' + __dirname + '/../../view/backup.html');
		ipcMain.on("downloadBackUp", (event) => { this.onClickDownload(); });
		ipcMain.on("continueBackUp", (event) => { this.onClickContinueLastDownload(); });
	}

	public async getThirdsFolders(folder: string): Promise<void> {
		if(ConfigDataServices.instance.isToDownload("ROOT")) {
			await this.ftpControlServices.getAllFiles([], 
				[BackupViewControl.FILTER_SHIP_NPC, 
				BackupViewControl.FILTER_FLEET, 
				BackupViewControl.FILTER_ASTEROID],
				ConfigDataServices.instance.getFtpFolderPath("ROOT"), 
				folder, 
				this.updatePercent);
		}
		if(ConfigDataServices.instance.isToDownload("DATA")) {
			await this.ftpControlServices.getAllFiles([], 
				[BackupViewControl.FILTER_DATA_SHIP_NPC, 
				BackupViewControl.FILTER_DATA_FLEET, 
				BackupViewControl.FILTER_DATA_ASTEROID],
				ConfigDataServices.instance.getFtpFolderPath("DATA"), 
				folder + BackupViewControl.LOCAL_BACKUP_DATA_FOLDER, 
				this.updatePercent);
		}
		if(ConfigDataServices.instance.isToDownload("INDEX")) {
			await this.ftpControlServices.getAllFiles([], 
				[],
				ConfigDataServices.instance.getFtpFolderPath("INDEX"), 
				folder + BackupViewControl.LOCAL_BACKUP_INDEX_FOLDER, 
				this.updatePercent);
		}

	}

	public onClickDownload(): void {
		const folder: string = FtpControlServices.LOCAL_BACKUP_FOLDER + "/" + this.commonControlServices.formatDate(new Date());
		this.logger.info("Event Download, folder: ");
		//TODO: Clean Progress Bar
		this.getThirdsFolders(folder);

	}

	public async onClickContinueLastDownload(): Promise<void> {
		if(ConfigDataServices.instance.isToDownload("ROOT")) {
			await this.ftpControlServices.continueLastBackUp([], 
				[BackupViewControl.FILTER_SHIP_NPC, 
				BackupViewControl.FILTER_FLEET, 
				BackupViewControl.FILTER_ASTEROID],
				ConfigDataServices.instance.getFtpFolderPath("ROOT"), 
				"", 
				this.updatePercent);
		}
		if(ConfigDataServices.instance.isToDownload("DATA")) {
			await this.ftpControlServices.continueLastBackUp([], 
				[BackupViewControl.FILTER_DATA_SHIP_NPC, 
				BackupViewControl.FILTER_DATA_FLEET, 
				BackupViewControl.FILTER_DATA_ASTEROID],
				ConfigDataServices.instance.getFtpFolderPath("DATA"), 
				BackupViewControl.LOCAL_BACKUP_DATA_FOLDER, 
				this.updatePercent);
		}
		if(ConfigDataServices.instance.isToDownload("INDEX")) {
			await this.ftpControlServices.continueLastBackUp([], 
				[],
				ConfigDataServices.instance.getFtpFolderPath("INDEX"), 
				BackupViewControl.LOCAL_BACKUP_INDEX_FOLDER, 
				this.updatePercent);
		}
	}

	public updatePercent(percent: number) {
		Main.mainWindow.webContents.send('percentUpdate', percent + "%");  
	}
}
