import CommonControlServices from "./internal/commonControlServices";
import FtpControlServices from "./internal/ftpControlServices";
import PlayerControlServices from "./internal/playerControlServices";
import EventControlServices from "./internal/eventControlServices";
import StatControlServices from "./internal/statControlServices";

import BackupViewControl from "./view/backupViewControl";
import SomeViewControl from "./view/someViewControl";


export default class ControlServicesLoader {	
	/** All Control Services */
	private commonControlService: CommonControlServices;
	private ftpControlService: FtpControlServices;
	private playerControlServices: PlayerControlServices;
	private eventControlServices: EventControlServices;
	private statControlServices: StatControlServices;

	public constructor () {}

	public loadControlServices(): void {
        this.commonControlService = new CommonControlServices();
        this.ftpControlService = new FtpControlServices(this.commonControlService);
        this.playerControlServices = new PlayerControlServices(this.commonControlService);
        this.eventControlServices = new EventControlServices(this.playerControlServices);
        this.statControlServices = new StatControlServices(this.playerControlServices, this.eventControlServices,
            this.commonControlService);
    }

    public launchApplication() {
    	this.loadControlServices();
    	const backupViewControl: BackupViewControl = new BackupViewControl(this.ftpControlService, this.commonControlService);
        new SomeViewControl(this.ftpControlService, this.statControlServices, backupViewControl).loadHtml();  
    }
}
