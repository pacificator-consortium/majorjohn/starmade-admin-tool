import * as fs from 'fs';
import { getLogger, Logger } from 'log4js';

import Event from './../../model/event';
import Player from './../../model/player';
import Session from './../../model/session';
import PlayerDataServices from './playerDataServices';
import StatControlServices from './statControlServices';
import CommonControlServices from './commonControlServices';

export default class PlayerControlServices {
	private logger: Logger;

	private playerDataServices: PlayerDataServices;
	private commonControlServices: CommonControlServices;

	constructor (commonControlServicesParam: CommonControlServices) {
		this.logger = getLogger("global");
		this.playerDataServices = new PlayerDataServices();
		this.commonControlServices = commonControlServicesParam;
	}

	public getPlayers(): Player[] {
		return this.playerDataServices.getPlayers();
	}

	/* Check if player exists, if it exist update and return, else create player */
	public retrievePlayer(accountName: string, pseudo: string): Player {
		var player: Player;
		var filteredPlayer: Player[] = this.getPlayers().filter(p => p.getAccountName() === accountName);
		if(typeof filteredPlayer !== 'undefined' && filteredPlayer.length > 0){
			player = filteredPlayer[0];
		} else {
			player = new Player(accountName);
			this.getPlayers().push(player);
		}
		if(player.getPseudos().indexOf(pseudo) === -1){
			player.getPseudos().push(pseudo);
		}
		return player;
	}

	public createPlayerFromLOG(line: string): Player {
		var data: string[] =line.split("RegisteredClient: ")[1].split("connected: ")[0].split(" ");
		var playerAccountName: string = data[2].substring(1, data[2].length - 1);
		var playerPseudo: string = data[0];
		return this.retrievePlayer(playerAccountName, playerPseudo);
	}

	public addEvents(player: Player, event: Event): void {
		var filteredEvent: Event[] = player.getEvents().filter(
			e => e.getEventDate().getTime() === event.getEventDate().getTime() && 
			e.getType() === event.getType());
		if(typeof filteredEvent !== 'undefined' && filteredEvent.length > 0) {
			this.logger.debug("Event Already Add");
		} else {
			player.getEvents().push(event);
		}
	}

	public createSession(player: Player): Session[] {
		var sortedLog: Event[] = player.getEvents()
		.filter(e => e.getType() === Event.LOGINTYPE || e.getType() === Event.LOGOUTTYPE)
		.sort((a,b) => a.getEventDate().getTime() - b.getEventDate().getTime() )
		var sessions: Session[] = [];
		for(var i=0; i < sortedLog.length - 1; i+=2) {
			sessions.push(new Session(sortedLog[i].getEventDate(), sortedLog[i+1].getEventDate()));
		}
		return sessions;
	}

	public writePlayer(player: Player, stream): void {
		this.logger.info("Writting Player " + player.getAccountName());
		player.getEvents().forEach(event => stream.write(event.getTrueLog() + '\n'))
	}

	public writePlayers(players: Player[]): void {
		this.logger.info("Writting Players ...");
		var stream = this.commonControlServices.getWriteStream(StatControlServices.LOCAL_DATA_FOLDER,
			StatControlServices.OUTPUT_FILE, true);
		stream.once('open', (fd) => {
			players.forEach(player => {
				this.writePlayer(player, stream);
			});
		});
	}

	public writeSessions(players: Player[]): void {
		this.logger.info("Writting Session ...");
		var stream = this.commonControlServices.getWriteStream(StatControlServices.LOCAL_DATA_FOLDER,
			StatControlServices.SESSION_FILE, true);
		this.getPlayers().forEach(player => {
			stream.write("Player: " + player.getPseudos() + '\n');
			this.createSession(player).forEach(session => stream.write(session.toString() + '\n'));
		});
	}

	/* TODO: Optimize */
	public cleanUseLessDataPlayer(player: Player): void {
		var sortedLog: Event[] = player.getEvents()
		.filter(e => e.getType() === Event.LOGINTYPE || e.getType() === Event.LOGOUTTYPE)
		.sort((a,b) => a.getEventDate().getTime() - b.getEventDate().getTime());
		var i: number = 0;
		while(i < sortedLog.length) {
			if(i+1 < sortedLog.length) {
				if(sortedLog[i].getType() === Event.LOGINTYPE && sortedLog[i+1].getType() === Event.LOGOUTTYPE) {
					i+=2;
				} else if(sortedLog[i].getType() === Event.LOGOUTTYPE && sortedLog[i+1].getType() === Event.LOGOUTTYPE) {
					this.removeEvent(player, sortedLog[i]);
					this.removeEvent(player, sortedLog[i+1]);
					sortedLog.splice(i,2);				
				} else {
					this.removeEvent(player, sortedLog[i]);
					sortedLog.splice(i,1);	
				} 
			} else {
				if(sortedLog[i].getType() === Event.LOGINTYPE) {
					this.removeEvent(player, sortedLog[i]);
					sortedLog.splice(i,1);
				}
			}
		}
	}

	public cleanUseLessData(players: Player[]): void {		
		this.logger.info("Cleaning data ...");
		players.forEach(player => this.cleanUseLessDataPlayer(player));
		this.logger.info("Cleaning data finish");
	}

	public removeEvent(player: Player, element: Event): void {
		player.getEvents().splice(player.getEvents().indexOf(element), 1);
	}
}
