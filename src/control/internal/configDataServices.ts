import * as Ftp from 'ftp';
import * as fs from 'fs';
import {dialog} from 'electron';
import { getLogger, Logger } from 'log4js';

export default class ConfigDataServices {	
	private static CONFIG_PATH: string = "./config/config.json";
	private static FOLDER_NAME: string[] = ["ROOT", "DATA", "INDEX"];

	private logger: Logger;
	private allConfig;

	public static readonly instance: ConfigDataServices = new ConfigDataServices();

	private constructor () {
		this.logger = getLogger("global");
		this.loadConfig();		
	}

	public loadConfig(): void {
		this.logger.info("Load Configuration ...");
		this.allConfig = JSON.parse(fs.readFileSync(ConfigDataServices.CONFIG_PATH).toString());
		this.logger.info("Load Configuration Finish");
	}

	/** Access methods to the config file */
	public getFtpFolderPath(folder: string): string {
		let path: string = "";
		if(ConfigDataServices.FOLDER_NAME.indexOf(folder) != -1) {
			path = this.allConfig["FTP_MAP_" + folder + "_FOLDER"];
		} else {
			this.logger.error("Ask path for folder: " + folder + " but this folder don't exist");
		}
		return path;
	}

	public isToDownload(folder: string): boolean {
		let download: boolean = false;
		if(ConfigDataServices.FOLDER_NAME.indexOf(folder) != -1) {
			download = this.allConfig["DOWNLOAD_" + folder + "_FOLDER"];
		}
		return download;
	}
}
