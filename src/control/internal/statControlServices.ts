import * as Readline from 'readline';
import * as fs from 'fs';
import { getLogger, Logger } from 'log4js';

import Event from './../../model/event';
import Player from './../../model/player';
import Session from './../../model/session';
import PlayerControlServices from './playerControlServices';
import EventControlServices from './eventControlServices';
import CommonControlServices from './commonControlServices';

export default class StatControlServices {

	public static LOG_START_INDEX: number = 22;
	public static LOCAL_DATA_FOLDER: string = "data/";
	public static OUTPUT_FILE = "stats.dc"
	public static TEMPORARY_FILE = "tmp.dc"
	public static SESSION_FILE = "session.dc"

	private static FILE_NAME_FILTER: string = "logstarmade";

	private logger: Logger;

	private playerControlServices: PlayerControlServices;
	private eventControlServices: EventControlServices;
	private commonControlServices: CommonControlServices;

	constructor(playerControlServicesParam: PlayerControlServices, 
		eventControlServicesParam: EventControlServices,
		commonControlServicesParam: CommonControlServices) {
		this.logger = getLogger("global");		
		this.playerControlServices = playerControlServicesParam;
		this.eventControlServices = eventControlServicesParam;
		this.commonControlServices = commonControlServicesParam;
	}

	public isLogLineToKeep(line: string): boolean {
		var toAnalyse = line.substring(StatControlServices.LOG_START_INDEX, line.length);
		return toAnalyse.startsWith(EventControlServices.LOGIN) || toAnalyse.startsWith(EventControlServices.LOGOUT);
	}

	public filterLogs(file: string, stream, cpt: string[], fileNumber: number) {
		var lineReader = Readline.createInterface({
			input: fs.createReadStream(file)
		});

		lineReader.on('line', line => {
			if(this.isLogLineToKeep(line)) {
				stream.write(line + '\n');
			}
		});

		lineReader.on('close', _ => {
			cpt.push(file);
			if(cpt.length === fileNumber) {
				stream.end();
			}
		});
	}

	public writeFilteredLogs(folderToRead: string) {
		fs.readdir(folderToRead, (err, files) => {
			//Don't read potential useless file
			var filteredList = files.filter(file => file.startsWith(StatControlServices.FILE_NAME_FILTER));

			var stream = this.commonControlServices.getWriteStream(StatControlServices.LOCAL_DATA_FOLDER,
				StatControlServices.TEMPORARY_FILE, true);

			var cpt: string[] = [];
			stream.once('open', (fd) => {
				filteredList.forEach(file => {
					this.logger.info("read file: " + file + " ...");
					this.filterLogs(folderToRead + file, stream, cpt, filteredList.length);
					this.logger.info("read file sucessfull.");
				});
			});

			stream.on('close', () => {
				this.parseLogs();
			})
		})
	}

	public parseLogs(): void {
		this.parseFileToPlayer(StatControlServices.OUTPUT_FILE, () => {
			this.parseFileToPlayer(StatControlServices.TEMPORARY_FILE, () => {	
				this.playerControlServices.cleanUseLessData(this.playerControlServices.getPlayers());
				this.playerControlServices.writeSessions(this.playerControlServices.getPlayers());
				this.playerControlServices.writePlayers(this.playerControlServices.getPlayers());
			});
		});
	} 

	public parseFileToPlayer(fileToRead, next: Function): void {
		this.logger.info("Parse File: " + fileToRead + " ...");

		var file = fs.createReadStream(StatControlServices.LOCAL_DATA_FOLDER + fileToRead)
		file.on('error', error => {
			this.logger.error(error);
			next();
		});

		var lineReader = Readline.createInterface({
			input: file
		});

		lineReader.on('line', line => {
			var date: string = line.substring(1, StatControlServices.LOG_START_INDEX - 2);
			var type: string = this.eventControlServices.getEventType(line);
			var player: Player = this.playerControlServices.createPlayerFromLOG(line);
			this.eventControlServices.createEventObj(player, date, type, line);
		});

		lineReader.on('close', () => {
			this.logger.info("Parse file successfull.");
			next();
		});
	} 
}
