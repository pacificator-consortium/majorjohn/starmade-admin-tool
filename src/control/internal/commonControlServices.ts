import * as fs from 'fs';
import {dialog} from 'electron';
import { getLogger, Logger } from 'log4js';


export default class commonControlServices {
	private logger: Logger;

	constructor() {
		this.logger = getLogger("global");
	}

	public getWriteStream(folder: string, fileName: string, createFolder: boolean) {
		var stream = null;
		try {
			if(createFolder) {
				fs.mkdirSync(folder);
				this.logger.info("Create folder: " + folder);
			}
		} catch (err) {
			if (err.code !== 'EEXIST') {
				this.logger.error(err);
				dialog.showErrorBox("Erreur",err);
			}
		}
		stream = fs.createWriteStream(folder + "/" + fileName);
		return stream;
	}

	public formatDate(d: Date): string {
		return [d.getFullYear(), d.getMonth()+1 , d.getDate(), d.getHours(), d.getMinutes(), d.getSeconds()].join('-');	
	}
}
