import * as Ftp from 'ftp';
import * as fs from 'fs';
import {dialog} from 'electron';
import { getLogger, Logger } from 'log4js';

export default class FtpDataServices {	
	private static FTP_AUTH_PATH: string = "./config/auth.json";

	private logger: Logger;
	private ftpClient;

	constructor () {
		this.logger = getLogger("global");
		this.ftpClient = new Ftp();
	}

	public connect(): void {
		try {
			this.logger.info("Read FTP Authentication ...")
			const authConfig = fs.readFileSync(FtpDataServices.FTP_AUTH_PATH).toString();
			this.logger.info("FTP Connection ...")
			this.ftpClient.connect(JSON.parse(authConfig));
			this.logger.info("Connection sucessfull ...")
		} catch (err) {
			this.logger.error(err);
			if(err.code === 'ENOENT') {
				dialog.showErrorBox("Erreur","Le fichier config/auth.json est introuvable");
			} else {
				throw err;
			}			
		}
	}

	public disconnect(): void {
		this.ftpClient.end();
	}

	public getFtpClient() {
		return this.ftpClient;
	}
}
