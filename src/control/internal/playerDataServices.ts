import Player from './../../model/player';

export default class PlayerDataServices {

	private players: Player[];

	constructor() {
		this.players = [];
	}

	public getPlayers(): Player[] {
		return this.players;
	}
}
