import * as fs from 'fs';
import { getLogger, Logger } from 'log4js';
import { ListingElement } from 'ftp';

import CommonControlServices from './commonControlServices';

import FtpDataService from './ftpDataServices';

export default class FtpControlServices {

	public static LOCAL_BACKUP_FOLDER: string = "backup";
	public static LOCAL_LOGS_FOLDER: string = "logs";
	public static FTP_LOGS_FOLDER: string = "Starmade/logs";
	public static LOG_NAME_FILTER: RegExp = /logstarmade\w*/;

	private ftpDataService: FtpDataService;
	private commonControlServices: CommonControlServices;
	private logger: Logger;
	private loggerFiles: Logger;
	private loggerError: Logger;

	constructor (commonControlServicesParam: CommonControlServices) {
		this.logger = getLogger("global");
		this.loggerError = getLogger("error");
		this.loggerFiles = getLogger("files");

		this.commonControlServices = commonControlServicesParam;
		this.ftpDataService = new FtpDataService();
		this.ftpDataService.connect();
	}

	public disconnect(): void {
		this.ftpDataService.disconnect();
	}

	public getFtpClient() {
		return this.ftpDataService.getFtpClient();
	}

	private getPercent(current: number, total: number): number {
		return Math.round(current / total * 100);
	}

	/**
	* Get the file list from the folder path on the ftp server
	* /!\ Connection must be established
	*/
	public getFileList(path: string): Promise<ListingElement[]> {
		this.logger.info("Getting file list ... " + path);
		return new Promise<ListingElement[]>((done, reject) => {
			this.getFtpClient().list("-a " + path, (err, list) => {
				if (err) { 
					this.loggerError.error(err);
					reject(err);
				} else {
					let filteredList: ListingElement[] = list.filter(f => f.name !== "." || f.name !== "..");
					this.logger.info(filteredList.length + " file found.");
					done(filteredList);
				}
			});
		});
	}

	public downloadfile(ftpPath: string, fileName: string, localPath: string): Promise<void> {
		let path: string = ftpPath + "/" + fileName;
		return new Promise<void>((done, reject) => {
			this.getFtpClient().get(path, (err, stream) => {
				if (err) { 
					this.loggerFiles.error("Error GET " + path);
					this.loggerError.error(err);
					reject(err);
				} else {
					this.loggerFiles.info(path + " start download.");

					stream.once('close', done);

					const writer = this.commonControlServices.getWriteStream(localPath, fileName, true);
					stream.pipe(writer);
				}
			});   
		});
	}

	/**
	* Filter specifique file. There is 3 way to filter:
	* 	- filterKeepList : Contains RegExp, if match we keep the file in the list
	*	- filterIgnoreList : Contains RegExp, if match we filter the file.
	* 	- filesListToRemove : Contains file name to filter
	*/
	private filterFileList(fileList: ListingElement[], filterKeepList: RegExp[], filterIgnoreList: RegExp[], filesListToRemove: String[]){
		let filteredList: ListingElement[] = fileList;
		this.logger.debug("Showing filter used ...");
		this.logger.debug("RegExp Keep (if match we keep the file in the list) :");
		this.logger.debug(filterKeepList.map(regExp => "\n\t- " + regExp.toString()).reduce((accumulator, value) => accumulator + value, ""));
		this.logger.debug("RegExp Ignore (if match we keep we filter the file) :");
		this.logger.debug(filterIgnoreList.map(regExp => "\n\t- " + regExp.toString()).reduce((accumulator, value) => accumulator + value, ""));
		this.logger.debug("File to remove (file name to filter) :");
		this.logger.debug(filesListToRemove.map(fileName => "\n\t- " + fileName).reduce((accumulator, value) => accumulator + value, ""));
		this.logger.info("Starting filter ...");

		if(filterKeepList.length !== 0 || filterIgnoreList.length !== 0 || filesListToRemove.length !== 0){
			filteredList = filteredList.filter(file => 
				(filterKeepList.length > 0 &&  filterKeepList.some(f => f.test(file.name))) ||
				(filterIgnoreList.length > 0 && filterIgnoreList.filter(f => f.test(file.name)).length === 0) ||
				(filesListToRemove.length > 0 && filesListToRemove.indexOf(file.name) === -1));
		}
			

		return filteredList;
	}


	public async getAllFiles2(filterKeepList: RegExp[], filterIgnoreList: RegExp[], filesListToRemove: String[], ftpPath: string, localPath: string, ihmCallBack: Function): Promise<void> {
		//TODO Uncatched error
		try {
			const fileList:ListingElement[] = await this.getFileList(ftpPath);
			const filteredList = this.filterFileList(fileList, filterKeepList, filterIgnoreList, filesListToRemove);

			let dowloadFailedFiles: string[] = [];
			let fileDownloaded: number = 0;	

			this.logger.info(filteredList.length + " will be download.");

			this.logger.info("Starting download ...");
			await Promise.all(filteredList.map(async (file) => {
				let logMsg: string = file.name + " download";
				try {
					await this.downloadfile(ftpPath, file.name, localPath);
					fileDownloaded++;
					logMsg += " Success";
				} catch (error) {
					dowloadFailedFiles.push(ftpPath + "/" + file.name);
					logMsg += " Failed";
				}
				let percent: number = this.getPercent(fileDownloaded,filteredList.length);
				logMsg+=". Status: " + percent + "%.";
				this.loggerFiles.info(logMsg);
				ihmCallBack(percent);
			}));

			this.logger.warn("List of failed download :");
			dowloadFailedFiles.forEach(file => this.logger.warn(file));

		} catch (error) {
			console.log(error)
		}

		return new Promise<void>(async (done, reject) => {
			done();
		});

	}

	//TODO To rename
	public async getAllFiles(filterKeepList: RegExp[], filterIgnoreList: RegExp[], ftpPath: string, localPath: string, ihmCallBack: Function): Promise<void> {
		this.getAllFiles2(filterKeepList, filterIgnoreList, [], ftpPath, localPath, ihmCallBack);
	}

	private getCurrentFolder(partLocalPath: string): Promise<string> {
		return new Promise<string>((done, reject) => {
			fs.readdir(FtpControlServices.LOCAL_BACKUP_FOLDER, (err, folders) => {
				folders.sort();
				let currentFolder: string = folders[folders.length - 1] + partLocalPath;				
				done(FtpControlServices.LOCAL_BACKUP_FOLDER + "/" + currentFolder);
			});			
		});
	}

	private getFilterIgnoreList(currentFolder: string ): Promise<string[]>  {
		return new Promise<string[]>((done, reject) => {
			fs.readdir(currentFolder, (err, files) => {
				if(err) {
					done([]);
				}
				else {
					done(files);
				}
			});		
		});			
	}


	public async continueLastBackUp(filterKeepList: RegExp[], filterIgnoreList: RegExp[], ftpPath: string, partLocalPath: string, ihmCallBack: Function): Promise<void> {
		this.logger.info("Find Last Back Up ...");
		let currentFolder: string = await this.getCurrentFolder(partLocalPath);
		this.logger.info("Folder find: " + currentFolder);
		let filesRegExp: string[] = await this.getFilterIgnoreList(currentFolder);
		return this.getAllFiles2(filterKeepList, filterIgnoreList, filesRegExp, ftpPath, currentFolder, ihmCallBack);
	}
}
