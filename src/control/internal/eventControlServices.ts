import Event from './../../model/event';
import Player from './../../model/player';
import Session from './../../model/session';
import PlayerControlServices from './playerControlServices';
import StatControlServices from './statControlServices';

export default class EventControlServices {

	public static LOGIN: string = "[SERVER][LOGIN] login received";
	public static LOGOUT: string = "[SERVER][DISCONNECT] Client 'RegisteredClient:"

	private playerControlServices: PlayerControlServices;

	constructor (playerControlServicesParam: PlayerControlServices) {
		this.playerControlServices = playerControlServicesParam;
	}

	public createEventObj(player: Player, date: string, type: string, line: string) {
		var event: Event = new Event(new Date(date), player, type, line);
		this.playerControlServices.addEvents(player,event);
	}

	public getEventType(line: string) {
		var toAnalyse: string = line.substring(StatControlServices.LOG_START_INDEX, line.length);
		var result: string = Event.UNKNOWTYPE;
		if(toAnalyse.startsWith(EventControlServices.LOGIN)) {
			result = Event.LOGINTYPE;
		}
		if(toAnalyse.startsWith(EventControlServices.LOGOUT)) {
			result = Event.LOGOUTTYPE;
		}
		return result;
	}
}
