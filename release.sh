#!/bin/bash
rm -rf dist
rm -rf release-build
cp -R src dist
./node_modules/typescript/bin/tsc
rm -rf dist/*.ts
cp package-release.json dist/package.json
cd dist
npm install
cd ..
./node_modules/electron-packager/cli.js ./dist starmade-admin-tool --overwrite --asar --platform=win32 --arch=ia32 --out=release-build
cp -r config release-build/starmade-admin-tool-win32-ia32/.
mv release-build/starmade-admin-tool-win32-ia32 release-build/starmade-admin-tool