# Projet d'un Client en relation avec le projet TemplateServer

Pour l'instation du projet :
```
npm install
npm run css-install
```

Pour compiler (output: dist) :
```
./compile.sh
```

Pour créer une release :
```
./release.sh
```

Pour lancer l'application :
```
node_modules/electron/cli.js .
```

Pour nettoyer le projet :
```
npm run clean
```

# To Do List: 

# [0.1.0] 
- [X] Set up project
- [X] Avoir une interface
- [X] Connexion au FTP
- [X] Récupération des logs
- [X] Tri des Logs
- [X] Parse Logs
- [X] Conservation de certaine donnée des logs
- [X] Ajout CSS
- [X] Logger
- [X] Barre de progression 
- [X] Réécriture des filtrage sur les sessions pour plus de robustesse (Croisement entre deux jeux de logs)
- [X] Création des dossiers nécessaire pour le stockage local des logs
- [X] Sauvegarde propre de tout les logs à conserver
- [X] Supression bouton inutile
- [X] formatage des Sessions dans un fichier

# [0.1.1]
- [ ] formatage des Sessions dans l'interface fichier
- [ ] barre de progression lors du process des statistique
- [ ] Affichage / Dé-Affichage des barres de progression
- [ ] Filtrage sur les pseudo [Option]
- [ ] Changer titre


# [0.2.0]
- [ ] Fonction de backUp
- [0] Système de filtre
- [X] Dossier séparé
- [ ] Compression
- [ ] IHM

# [0.X.0]
- [ ] Choix du chemin du ftp
- [ ] Création des maquettes
- [ ] Internazionalisation
- [ ] Supression d'un répertoire
- [ ] Ftp Disconnect - Connect - Reconnect
- [ ] Decrypter les fichiers du jeu

# [?]
- [ ] Script pour backup automatique

# Problème connu :
- [X] [FIXED] Lors de la production d'une release les dépendances ne sont pas embarqués.
- [X] [FIXED] Le dossier config est embarqué dans le .asar